from random import randint

class Function():
    """
    The function for calculating the volume of a rectangle
    """
    def __init__(self):
        """Setting the parameters"""
        self.a = randint(1, 5)
        self.b = randint(1, 5)
        self.c = randint(0, 5)

    def find_volume(self):
        """The function for calculating the volume of a rectangle"""
        self.__init__()
        volume = self.a * self.b * self.c
        return volume

    def find_diagonal(self):
        """Function for finding the diagonal of a parallelepiped"""
        diagonal = ((self.a**2) + (self.b**2) + (self.c**2))**0.5
        return diagonal

