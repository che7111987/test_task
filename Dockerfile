FROM ubuntu
FROM python
WORKDIR /code
COPY requirements.txt .
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt
COPY . /code
EXPOSE 5000
CMD ["pytest", "./tests.py"]