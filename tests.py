import pytest
from function import Function

# pytest -s -vv tests.py

@pytest.mark.xfail
def test_volume_zero():
    """Checking for zero"""
    figure = Function()
    assert figure.find_volume() != 0, "The figure does not exist"

def test_height():
    """Checking the height"""
    figure = Function()
    assert figure.a != 0, "The height do not be zero"

def test_length():
    """Checking the length"""
    figure = Function()
    assert figure.b != 0, "The length do not be zero"

@pytest.mark.xfail
def test_width():
    """Checking the wigth"""
    figure = Function()
    assert figure.c != 0, "The width do not be zero"

def test_diagonal():
    """Finding the diagonal"""
    figure = Function()
    assert figure.find_diagonal() != 4.123105625617661, "Congratulations! The diagonal is 4.123105625617661!"

